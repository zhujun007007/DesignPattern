/*
    代理模式 可以详细控制访问某个类（对象）的方法，在调用这个方法前作的前置处理（统一的流程代码放到代理中处理）。调用这个方法后做后置处理。
    例如：明星的经纪人，租房的中介等等都是代理。

    静态代理模式一般会有三个角色：

	抽象角色：指代理角色（经纪人）和真实角色（明星）对外提供的公共方法，一般为一个接口

	真实角色：需要实现抽象角色接口，定义了真实角色所要实现的业务逻辑，以便供代理角色调用。也就是真正的业务逻辑在此。

	代理角色：需要实现抽象角色接口，是真实角色的代理，通过真实角色的业务逻辑方法来实现抽象方法，并可以附加自己的操作。

	将统一的流程控制都放到代理角色中处理！
*/

#include <iostream>
using namespace std;

// 抽象角色
class Subject 
{
public:
    virtual void Request() = 0;
    virtual ~Subject() { }
};

// 真实角色
class ConcreteSubject :public Subject
{
public:
    virtual void Request() override
	{
        cout << "ConcreteSubject::Request..." << endl;
    }
};

// 代理角色
class Proxy : public Subject
{
public:
    Proxy(Subject* subject)
	{
        _sub = subject;
    }

    virtual void Request() override
	{
        cout << "Proxy::Request start..." << endl;
        _sub->Request();
		cout << "Proxy::Request end..." << endl;
    }

private:
    Subject *_sub;
};

int main() 
{
    Subject* sub = new ConcreteSubject();
    Subject* proxy = new Proxy(sub);
    proxy->Request();

    delete sub;
    delete proxy;

    return 0;
}
