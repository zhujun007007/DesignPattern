/*
四个角色：抽象状态类(State)、具体状态类(ConcreateState)、情景类(Context)、客户端(Client) 

抽象状态类(State):提供一个与情景类有关的State行为。

具体状态类(ConcreateState):实现这个行为，实现一个状态。

情景类(Context):维护一个State的实例对象，并且提供一个客户操作置换状态的接口。

客户端(Client):直接调用情景类操作即可。

实现思路：直接调用情景类，然后在情景类自动操作或者手动操作置换状态，并且取得不同结果。

应用场景：文件提交审核入库。

分析：文件创建，然后提交审核，审核通过再入库，入库保存完成进入下一流程。
*/

#include <iostream>
#include <memory>
using namespace std;

class FileSub;

//抽象状态类(State)
class State
{
public:
	virtual void Submit(FileSub* filesub) = 0; 
	virtual ~State(){ }
};

//具体状态类(ConcreateState)
class BeginState : public State
{
public:
	virtual void Submit(FileSub* filesub) override;
};

//具体状态类(ConcreateState)
class CheckState : public State
{
public:
	virtual void Submit(FileSub* filesub) override;
};

//具体状态类(ConcreateState)
class StorageState : public State
{
public:
	virtual void Submit(FileSub* filesub) override;
};

//具体状态类(ConcreateState)
class CompleteState : public State
{
public:
	virtual void Submit(FileSub* filesub) override;
};

//情景类(Context)
class FileSub
{
	friend class BeginState;
	friend class CheckState;
	friend class StorageState;
	friend class CompleteState;
public:
	FileSub()
	{
		state = std::make_shared<BeginState>();
	}

	void Submit()
	{
		state->Submit(this);
	}

protected:
	void SetState(std::shared_ptr<State> s)
	{
		state = s;
	}

private:
	std::shared_ptr<State> state;
};

void BeginState::Submit(FileSub* filesub)
{
	cout << "创建文档，提交待审核。" << endl;
	filesub->SetState(std::make_shared<CheckState>());
}

 void CheckState::Submit(FileSub* filesub)
{
	cout << "审核文档，准备入库" << endl;
	filesub->SetState(std::make_shared<StorageState>());
}

void StorageState::Submit(FileSub* filesub)
{
	 cout << "入库完成，准备结束此流程" << endl;
	 filesub->SetState(std::make_shared<CompleteState>());
}

void CompleteState::Submit(FileSub* filesub)
{
	cout << "结束此流程，准备创建下一个文件" << endl;
	filesub->SetState(std::make_shared<BeginState>());
}

void main()
{
	FileSub* file = new FileSub();
	file->Submit();
	file->Submit();
	file->Submit();
	file->Submit();
	delete file;
}