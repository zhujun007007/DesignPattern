#include <iostream>
#include <mutex>
using namespace std;

// 单例模式--懒汉模式 线程安全
class Singleton 
{
public:
	static Singleton* Instance()
	{
		// 先判断不为空，这么做的原因是高并发的时候，避免访问锁，造成大量性能开销。
		if(_instance)
			return _instance;
		else
		{
			std::lock_guard<std::mutex> lock(_instance_mutex);
			// 防止多个线程同时运行到这里，所以加完锁之后，这里需要再次判断一下
			if(!_instance)
				_instance = new Singleton();

		}
		return _instance;
	}

	void unInstance()
	{
		if(!_instance)
			return;

		std::lock_guard<std::mutex> lock(_instance_mutex);
		if(_instance)
		{
			delete _instance;
			_instance = nullptr;
		}
	}

private:
	Singleton() 
	{
		cout << "Singleton..." << endl; 
	}

	// 禁止显示调用delete删除 
	void operator delete(void* p)
	{
		::delete p;
	}

private:
	static std::mutex _instance_mutex;
	static Singleton* _instance;
};
Singleton* Singleton::_instance = nullptr;
std::mutex Singleton::_instance_mutex;

int main() 
{
	Singleton *singleton1 = Singleton::Instance();
	cout << singleton1 << endl;

	Singleton *singleton2 = Singleton::Instance();
	cout << singleton2 << endl;

	Singleton::Instance()->unInstance();

	return 0;
}
